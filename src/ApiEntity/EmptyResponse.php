<?php

declare(strict_types=1);

namespace App\ApiEntity;

final class EmptyResponse implements ApiResponse
{
}
