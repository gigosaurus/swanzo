<?php

declare(strict_types=1);

namespace App\ApiEntity\Swarm;

class Checkin
{
    public string $id = '';
    public int $createdAt = 0;
    public Venue $venue;

    public function __construct()
    {
        $this->venue = new Venue();
    }
}
