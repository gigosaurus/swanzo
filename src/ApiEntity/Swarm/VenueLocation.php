<?php

declare(strict_types=1);

namespace App\ApiEntity\Swarm;

class VenueLocation
{
    public ?string $address = null;
    public float $lat = 0;
    public float $lng = 0;
    public ?string $postalCode = null;
    public ?string $city = null;
    public ?string $state = null;
    public ?string $country = null;
    /** @var String[] $formattedAddress */
    public array $formattedAddress = [];
}
