<?php

declare(strict_types=1);

namespace App\ApiEntity\Swarm;

class Checkins
{
    /** @var Checkin[] $items */
    public array $items = [];
}
