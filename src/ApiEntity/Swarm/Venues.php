<?php

declare(strict_types=1);

namespace App\ApiEntity\Swarm;

class Venues
{
    /** @var Venue[] $venues */
    public array $venues = [];
}
