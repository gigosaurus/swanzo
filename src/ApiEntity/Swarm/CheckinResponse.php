<?php

declare(strict_types=1);

namespace App\ApiEntity\Swarm;

class CheckinResponse
{
    public Checkin $checkin;

    public function __construct()
    {
        $this->checkin = new Checkin();
    }
}
