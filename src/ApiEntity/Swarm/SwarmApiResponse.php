<?php

declare(strict_types=1);

namespace App\ApiEntity\Swarm;

use App\ApiEntity\ApiResponse;

class SwarmApiResponse implements ApiResponse
{
    public ?string $access_token = null;
    public Venues|Checkins|CheckinResponse|null $response = null;
}
