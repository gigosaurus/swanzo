<?php

declare(strict_types=1);

namespace App\ApiEntity\Swarm;

class Venue
{
    public string $id = '';
    public string $name = '';
    public VenueLocation $location;
    /** @var Category[] $categories */
    public array $categories = [];

    public function __construct()
    {
        $this->location = new VenueLocation();
    }
}
