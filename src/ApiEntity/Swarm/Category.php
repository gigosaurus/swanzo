<?php

declare(strict_types=1);

namespace App\ApiEntity\Swarm;

class Category
{
    public string $name = '';
}
