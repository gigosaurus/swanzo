<?php

declare(strict_types=1);

namespace App\ApiEntity\Monzo;

class MerchantAddress
{
    public ?string $short_formatted = null;
    public ?string $formatted = null;
    public ?string $address = null;
    public ?string $city = null;
    public ?string $region = null;
    public ?string $country = null;
    public ?string $postcode = null;
    public float $latitude = 0;
    public float $longitude = 0;
}
