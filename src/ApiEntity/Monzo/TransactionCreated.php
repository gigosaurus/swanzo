<?php

declare(strict_types=1);

namespace App\ApiEntity\Monzo;

class TransactionCreated
{
    public string $id = '';
    public ?Merchant $merchant = null;
    public bool $is_load = false;
    public bool $originator = false;
    public bool $include_in_spending = true;
}
