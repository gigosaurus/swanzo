<?php

declare(strict_types=1);

namespace App\ApiEntity\Monzo;

class Webhook
{
    public string $account_id = '';
    public string $id = '';
    public string $url = '';
}
