<?php

declare(strict_types=1);

namespace App\ApiEntity\Monzo;

class Account
{
    public string $id = '';
    public string $description = '';
    public string $type = '';
    /** @var AccountOwner[] $owners */
    public array $owners = [];
}
