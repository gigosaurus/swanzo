<?php

declare(strict_types=1);

namespace App\ApiEntity\Monzo;

class AccountOwner
{
    public string $user_id = '';
    public string $preferred_name = '';
}
