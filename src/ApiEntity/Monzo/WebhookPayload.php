<?php

declare(strict_types=1);

namespace App\ApiEntity\Monzo;

use App\ApiEntity\ApiResponse;

class WebhookPayload implements ApiResponse
{
    public string $type = '';
    public ?TransactionCreated $data = null;
}
