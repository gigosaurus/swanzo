<?php

declare(strict_types=1);

namespace App\ApiEntity\Monzo;

use App\ApiEntity\ApiResponse;

class MonzoApiResponse implements ApiResponse
{
    // accounts
    /** @var Account[]|null $accounts */
    public ?array $accounts = null;

    // webhook
    public ?Webhook $webhook = null;

    // error
    public ?string $code = null;

    // oauth2/token
    public ?string $access_token = null;
    public ?string $refresh_token = null;
}
