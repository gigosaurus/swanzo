<?php

declare(strict_types=1);

namespace App\ApiEntity\Monzo;

class Merchant
{
    public string $id = '';
    public string $name = '';
    public string $logo = '';
    public string $emoji = '';
    public string $category = '';
    public bool $atm = false;
    public ?MerchantAddress $address = null;
}
