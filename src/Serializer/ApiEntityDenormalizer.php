<?php

declare(strict_types=1);

namespace App\Serializer;

use App\ApiEntity\ApiResponse;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use TypeError;

class ApiEntityDenormalizer implements DenormalizerInterface
{
    private PropertyInfoExtractor $propertyInfoExtractor;

    public function __construct()
    {
        $this->propertyInfoExtractor = new PropertyInfoExtractor(
            typeExtractors: [new PhpDocExtractor(), new ReflectionExtractor()]
        );
    }

    /**
     * @param array $data
     * @param class-string<ApiResponse> $type
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = []): object
    {
        return $this->denormalizeObject(new $type(), $data);
    }

    private function denormalizeObject(object $object, array $data, bool $strict = false): object
    {
        /**
         * @var string|int $key
         * @var mixed $value
         */
        foreach ($data as $key => $value) {
            if (is_string($key) && property_exists($object, $key)) {
                $types = $this->propertyInfoExtractor->getTypes($object::class, $key);
                if ($types === null) {
                    throw new NotNormalizableValueException(sprintf(
                        'Property %s of class %s is missing a type declaration',
                        $key,
                        $object::class
                    ));
                }

                $object->{$key} = $this->attemptItemDenormalization($types, $value, $strict);
            } elseif ($strict) {
                throw new UnexpectedValueException();
            }
        }
        return $object;
    }

    private function attemptDenormalization(
        Type $type,
        mixed $value,
        bool $strict,
    ): string|int|bool|array|null|object|float {
        switch ($type->getBuiltinType()) {
            case 'object':
                if (!is_array($value)) {
                    throw new UnexpectedValueException();
                }
                /**
                 * @var array<string, mixed> $value
                 * @var class-string<ApiResponse> $class
                 */
                $class = $type->getClassName();
                return $this->denormalizeObject(new $class(), $value, $strict);
            case 'array':
                if (!is_array($value)) {
                    throw new UnexpectedValueException();
                }
                $types = $type->getCollectionValueTypes();
                return array_map(fn(mixed $item): string|int|bool|array|null|object|float => $this
                    ->attemptItemDenormalization($types, $item, $strict), $value);
            case 'bool':
                if (!is_scalar($value)) {
                    throw new UnexpectedValueException();
                }
                return (bool) $value;
            case 'int':
                if (!is_scalar($value)) {
                    throw new UnexpectedValueException();
                }
                return (int) $value;
            case 'float':
                if (!is_scalar($value)) {
                    throw new UnexpectedValueException();
                }
                return (float) $value;
            case 'string':
                if (!is_scalar($value)) {
                    throw new UnexpectedValueException();
                }
                return (string) $value;
            case 'null':
                if ($value !== null) {
                    throw new UnexpectedValueException();
                }
                return null;
            default:
                throw new UnexpectedValueException();
        }
    }

    /**
     * @param Type[] $types
     */
    private function attemptItemDenormalization(
        array $types,
        mixed $item,
        bool $strict,
    ): string|array|bool|int|null|object|float {
        foreach ($types as $collectionType) {
            try {
                return $this->attemptDenormalization($collectionType, $item, true);
            } catch (UnexpectedValueException | TypeError) {
            }
        }

        if (!$strict) {
            foreach ($types as $collectionType) {
                try {
                    return $this->attemptDenormalization($collectionType, $item, false);
                } catch (UnexpectedValueException | TypeError) {
                }
            }
        }
        throw new UnexpectedValueException();
    }

    public function supportsDenormalization($data, string $type, ?string $format = null): bool
    {
        return is_array($data) && is_a($type, ApiResponse::class, true) && $format === 'json';
    }
}
