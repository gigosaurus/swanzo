<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\MerchantVenueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MerchantVenueRepository::class)
 */
class MerchantVenue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\OneToOne(targetEntity=Merchant::class, inversedBy="venue", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private Merchant $merchant;

    /**
     * @ORM\ManyToOne(targetEntity=Venue::class, inversedBy="merchants")
     * @ORM\JoinColumn(nullable=false)
     */
    private Venue $venue;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $valid = true;

    public function __construct()
    {
        $this->merchant = new Merchant();
        $this->venue = new Venue();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMerchant(): Merchant
    {
        return $this->merchant;
    }

    public function setMerchant(Merchant $merchant): self
    {
        $this->merchant = $merchant;

        return $this;
    }

    public function getVenue(): Venue
    {
        return $this->venue;
    }

    public function setVenue(Venue $venue): self
    {
        $this->venue = $venue;

        return $this;
    }

    public function getValid(): ?bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): self
    {
        $this->valid = $valid;

        return $this;
    }
}
