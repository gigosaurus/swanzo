<?php

declare(strict_types=1);

namespace App\Entity;

use App\ApiEntity\Swarm\Category;
use App\ApiEntity\Swarm\Venue as ApiVenue;
use App\Repository\VenueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VenueRepository::class)
 */
class Venue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private string $venueId = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $category = '';

    /**
     * @ORM\OneToOne(targetEntity=Address::class, inversedBy="venue", cascade={"persist", "remove"}, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Address $address = null;

    /**
     * @ORM\OneToMany(targetEntity=MerchantVenue::class, mappedBy="venue", orphanRemoval=true)
     */
    private Collection $merchants;

    public function __construct()
    {
        $this->merchants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVenueId(): string
    {
        return $this->venueId;
    }

    public function setVenueId(string $venueId): self
    {
        $this->venueId = $venueId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection|MerchantVenue[]
     */
    public function getMerchants(): array|Collection
    {
        return $this->merchants;
    }

    public function addMerchant(MerchantVenue $merchant): self
    {
        if (!$this->merchants->contains($merchant)) {
            $this->merchants[] = $merchant;
            $merchant->setVenue($this);
        }

        return $this;
    }

    public function removeMerchant(MerchantVenue $merchant): self
    {
        $this->merchants->removeElement($merchant);

        return $this;
    }

    public static function fromApiResponse(ApiVenue $venue): Venue
    {
        return (new Venue())->updateFromData($venue);
    }

    public function updateFromData(ApiVenue $venue): Venue
    {
        return $this
            ->setVenueId($venue->id)
            ->setName($venue->name)
            ->setCategory((reset($venue->categories) ?: new Category())->name)
            ->setAddress(($this->getAddress() ?? new Address())->updateFromSwarm($venue->location));
    }
}
