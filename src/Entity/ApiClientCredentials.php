<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ApiClientCredentialsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ApiClientCredentialsRepository::class)
 */
class ApiClientCredentials
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $access_token = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $refresh_token = null;

    /**
     * @ORM\OneToOne(targetEntity=ApiClient::class, mappedBy="credentials", cascade={"persist", "remove"})
     */
    private ?ApiClient $client = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccessToken(): ?string
    {
        return $this->access_token;
    }

    public function setAccessToken(string $access_token): self
    {
        $this->access_token = $access_token;

        return $this;
    }

    public function getRefreshToken(): ?string
    {
        return $this->refresh_token;
    }

    public function setRefreshToken(?string $refresh_token): self
    {
        $this->refresh_token = $refresh_token;

        return $this;
    }

    public function getClient(): ?ApiClient
    {
        return $this->client;
    }

    public function setClient(?ApiClient $client): self
    {
        // unset the owning side of the relation if necessary
        if ($client === null && $this->client !== null) {
            $this->client->setCredentials(null);
        }

        // set the owning side of the relation if necessary
        if ($client !== null && $client->getCredentials() !== $this) {
            $client->setCredentials($this);
        }

        $this->client = $client;

        return $this;
    }
}
