<?php

declare(strict_types=1);

namespace App\Entity;

use App\ApiEntity\Monzo\Merchant as ApiMerchant;
use App\Repository\MerchantRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MerchantRepository::class)
 */
class Merchant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private string $merchantId = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $logo = null;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private ?string $emoji = null;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private string $category = '';

    /**
     * @ORM\OneToOne(targetEntity=Address::class, inversedBy="merchant", cascade={"persist", "remove"}, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Address $address = null;

    /**
     * @ORM\OneToOne(targetEntity=MerchantVenue::class, mappedBy="merchant", cascade={"persist", "remove"})
     */
    private ?MerchantVenue $venue = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMerchantId(): string
    {
        return $this->merchantId;
    }

    public function setMerchantId(string $merchantId): self
    {
        $this->merchantId = $merchantId;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getEmoji(): ?string
    {
        return $this->emoji;
    }

    public function setEmoji(?string $emoji): self
    {
        $this->emoji = $emoji;

        return $this;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getVenue(): ?MerchantVenue
    {
        return $this->venue;
    }

    public function setVenue(MerchantVenue $venue): self
    {
        // set the owning side of the relation if necessary
        if ($venue->getMerchant() !== $this) {
            $venue->setMerchant($this);
        }

        $this->venue = $venue;

        return $this;
    }

    public static function fromApiResponse(ApiMerchant $merchant): Merchant
    {
        return (new Merchant())->updateFromData($merchant);
    }

    public function updateFromData(ApiMerchant $merchant): self
    {
        $this
            ->setMerchantId($merchant->id)
            ->setName($merchant->name)
            ->setLogo($merchant->logo ?: null)
            ->setEmoji($merchant->emoji ?: null)
            ->setCategory($merchant->category);
        if ($merchant->address !== null) {
            $this->setAddress(($this->getAddress() ?? new Address())->updateFromMonzo($merchant->address));
        }
        return $this;
    }
}
