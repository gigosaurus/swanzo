<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ApiClientRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ApiClientRepository::class)
 */
class ApiClient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $service = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $client_id = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $client_secret = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $state = '';

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = false;

    /**
     * @ORM\OneToOne(targetEntity=ApiClientCredentials::class, inversedBy="client", cascade={"persist", "remove"})
     */
    private ?ApiClientCredentials $credentials = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getService(): string
    {
        return $this->service;
    }

    public function setService(string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getClientId(): string
    {
        return $this->client_id;
    }

    public function setClientId(string $client_id): self
    {
        $this->client_id = $client_id;

        return $this;
    }

    public function getClientSecret(): string
    {
        return $this->client_secret;
    }

    public function setClientSecret(string $client_secret): self
    {
        $this->client_secret = $client_secret;

        return $this;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCredentials(): ?ApiClientCredentials
    {
        return $this->credentials;
    }

    public function setCredentials(?ApiClientCredentials $credentials): self
    {
        $this->credentials = $credentials;

        return $this;
    }
}
