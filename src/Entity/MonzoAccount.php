<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\MonzoAccountRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MonzoAccountRepository::class)
 */
class MonzoAccount
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $accountId = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $description = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $type = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $webhookId = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    public function setAccountId(string $accountId): self
    {
        $this->accountId = $accountId;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getWebhookId(): ?string
    {
        return $this->webhookId;
    }

    public function setWebhookId(?string $webhookId): self
    {
        $this->webhookId = $webhookId;

        return $this;
    }
}
