<?php

declare(strict_types=1);

namespace App\Entity;

use App\ApiEntity\Monzo\MerchantAddress;
use App\ApiEntity\Swarm\VenueLocation;
use App\Repository\AddressRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AddressRepository::class)
 * @ORM\Table(indexes={@ORM\Index(columns={"latitude","longitude"})})
 */
class Address
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $formatted = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $address = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $city = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $region = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $country = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $postcode = null;

    /**
     * @ORM\Column(type="float")
     */
    private float $latitude = 0;

    /**
     * @ORM\Column(type="float")
     */
    private float $longitude = 0;

    /**
     * @ORM\OneToOne(targetEntity=Merchant::class, mappedBy="address", cascade={"persist", "remove"}, fetch="EAGER")
     */
    private ?Merchant $merchant = null;

    /**
     * @ORM\OneToOne(targetEntity=Venue::class, mappedBy="address", cascade={"persist", "remove"}, fetch="EAGER")
     */
    private ?Venue $venue = null;

    /** @var array<int, float> $distanceFrom */
    private array $distanceFrom = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFormatted(): string
    {
        return $this->formatted;
    }

    public function setFormatted(string $formatted): self
    {
        $this->formatted = $formatted;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(?string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = round($latitude, 7);

        return $this;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = round($longitude, 7);

        return $this;
    }

    public function getMerchant(): ?Merchant
    {
        return $this->merchant;
    }

    public function setMerchant(Merchant $merchant): self
    {
        // set the owning side of the relation if necessary
        if ($merchant->getAddress() !== $this) {
            $merchant->setAddress($this);
        }

        $this->merchant = $merchant;

        return $this;
    }

    public function getVenue(): ?Venue
    {
        return $this->venue;
    }

    public function setVenue(Venue $venue): self
    {
        // set the owning side of the relation if necessary
        if ($venue->getAddress() !== $this) {
            $venue->setAddress($this);
        }

        $this->venue = $venue;

        return $this;
    }

    public function getDistanceFrom(Address $address): float
    {
        $id = $address->getId();
        if ($id !== null && array_key_exists($id, $this->distanceFrom)) {
            return $this->distanceFrom[$id];
        }
        $deltaLat = deg2rad(abs($address->getLatitude() - $this->getLatitude()));
        $deltaLng = deg2rad(abs($address->getLongitude() - $this->getLongitude()));
        $meanLat = deg2rad(($address->getLatitude() + $this->getLatitude()) / 2);
        $distance = round(6371009 * sqrt(pow($deltaLat, 2) + pow(cos($meanLat) * $deltaLng, 2)), 2);
        if ($id !== null) {
            $this->distanceFrom[$id] = $distance;
        }
        return $distance;
    }

    public static function fromMonzoResponse(MerchantAddress $address): Address
    {
        return (new Address())->updateFromMonzo($address);
    }

    public static function fromSwarmResponse(VenueLocation $location): Address
    {
        return (new Address())->updateFromSwarm($location);
    }

    public function updateFromMonzo(MerchantAddress $address): self
    {
        return $this
            ->setAddress($address->address)
            ->setCity($address->city)
            ->setCountry($address->country)
            ->setRegion($address->region)
            ->setPostcode($address->postcode)
            ->setFormatted($address->formatted ?? '')
            ->setLatitude($address->latitude)
            ->setLongitude($address->longitude);
    }

    public function updateFromSwarm(VenueLocation $location): self
    {
        return $this
            ->setAddress($location->address)
            ->setCity($location->city)
            ->setCountry($location->country)
            ->setRegion($location->state)
            ->setPostcode($location->postalCode)
            ->setFormatted(implode(', ', $location->formattedAddress))
            ->setLatitude($location->lat)
            ->setLongitude($location->lng);
    }
}
