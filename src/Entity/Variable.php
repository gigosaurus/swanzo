<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\VariableRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VariableRepository::class)
 */
class Variable
{
    public const CHECKIN_COOLDOWN = 'checkin_cooldown';
    public const MAX_VENUE_DISTANCE_FROM_MERCHANT = 'venue_radius';
    public const NEARBY_VENUES = 'nearby_venues';

    public const DEFAULTS = [
        self::CHECKIN_COOLDOWN => 18000, // 5 hours
        self::MAX_VENUE_DISTANCE_FROM_MERCHANT => 50,
        self::NEARBY_VENUES => 100,
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private string $key = '';

    /**
     * @ORM\Column(type="integer")
     */
    private int $value = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }
}
