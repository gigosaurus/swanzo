<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Address;
use App\Entity\Merchant;
use App\Repository\AddressRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/merchant')]
class MerchantController extends AbstractController
{
    #[Route('/{id}', name: 'merchant_show', methods: ['GET'])]
    public function show(Merchant $merchant, AddressRepository $addressRepository): Response
    {
        $address = $merchant->getAddress();
        $nearby = [];
        if ($address !== null) {
            $nearby = array_filter(
                $addressRepository->findNearby($address),
                static fn (Address $a) => $a->getVenue() !== null
            );
            usort(
                $nearby,
                static fn (Address $a, Address $b) => $a->getDistanceFrom($address) <=> $b->getDistanceFrom($address)
            );
        }
        return $this->render('merchant/show.html.twig', [
            'merchant' => $merchant,
            'nearby' => $nearby,
        ]);
    }
}
