<?php

declare(strict_types=1);

namespace App\Controller;

use App\ApiEntity\Swarm\Venue as SwarmVenue;
use App\Entity\Address;
use App\Entity\Variable;
use App\Entity\Venue;
use App\Repository\AddressRepository;
use App\Repository\ApiClientRepository;
use App\Repository\VariableRepository;
use App\Repository\VenueRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/venue')]
class VenueController extends AbstractController
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    #[Route('/{id}', name: 'venue_show', methods: ['GET'])]
    public function show(Venue $venue, AddressRepository $addressRepository): Response
    {
        $address = $venue->getAddress();
        $nearby = [];
        if ($address !== null) {
            $nearby = array_filter(
                $addressRepository->findNearby($address),
                static fn (Address $a) => $a->getVenue() !== null
            );
            usort(
                $nearby,
                static fn(Address $a, Address $b) => $a->getDistanceFrom($address) <=> $b->getDistanceFrom($address)
            );
        }
        return $this->render('venue/show.html.twig', [
            'venue' => $venue,
            'nearby' => $nearby,
        ]);
    }

    #[Route('/{id}/nearby', name: 'venues_nearby', methods: ['GET'])]
    public function nearby(
        Request $request,
        Address $address,
        ApiClientRepository $apiClientRepository,
        VenueRepository $venueRepository,
        VariableRepository $variableRepository,
    ): Response {
        $this->logger->info('Searching for nearby venues');
        $swarmApi = $apiClientRepository->getSwarmApi();
        if ($swarmApi === null) {
            throw new BadRequestHttpException("Swarm API is not set up!");
        }
        $swarmVenues = $swarmApi->venueSearch(
            $address->getLatitude(),
            $address->getLongitude(),
            limit: $variableRepository->get(Variable::NEARBY_VENUES)
        )->response?->venues ?? [];

        $this->logger->debug('Fetching pre-existing venues');
        $venues = $venueRepository->findByIds(array_map(
            static fn (SwarmVenue $swarmVenue): string => $swarmVenue->id,
            $swarmVenues
        ));
        $venuesById = [];
        foreach ($venues as $venue) {
            $venuesById[$venue->getVenueId()] = $venue;
        }

        $this->logger->debug('Updating database', [
            'new_venues' => count($swarmVenues) - count($venues),
            'total' => count($swarmVenues),
        ]);

        $em = $this->getDoctrine()->getManager();
        foreach ($swarmVenues as $swarmVenue) {
            if (array_key_exists($swarmVenue->id, $venuesById)) {
                $venue = $venuesById[$swarmVenue->id];
                $venue->updateFromData($swarmVenue);
            } else {
                $venue = Venue::fromApiResponse($swarmVenue);
                $em->persist($venue);
            }
        }
        $em->flush();

        return $this->redirect($request->headers->get('referer') ?? $this->generateUrl('mapping_index'));
    }
}
