<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Merchant;
use App\Entity\MerchantVenue;
use App\Entity\Venue;
use App\Repository\MerchantVenueRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/mapping')]
class MappingController extends AbstractController
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    #[Route('/', name: 'mapping_index', methods: ['GET'])]
    public function index(MerchantVenueRepository $merchantVenueRepository): Response
    {
        return $this->render('mapping/index.html.twig', [
            'merchant_venues' => $merchantVenueRepository->findAll(),
        ]);
    }

    #[Route('/{id}', name: 'mapping_show', methods: ['GET'])]
    public function show(MerchantVenue $merchantVenue): Response
    {
        return $this->render('mapping/show.html.twig', [
            'merchant_venue' => $merchantVenue,
        ]);
    }

    #[Route('/{id}/enable', name: 'mapping_enable', methods: ['GET'])]
    public function enable(Request $request, MerchantVenue $merchantVenue): Response
    {
        $this->logger->info('Enabling merchant venue mapping', ['id' => $merchantVenue->getId()]);
        $merchantVenue->setValid(true);

        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($request->headers->get('referer') ?? $this->generateUrl('mapping_index'));
    }

    #[Route('/{id}/disable', name: 'mapping_disable', methods: ['GET'])]
    public function disable(Request $request, MerchantVenue $merchantVenue): Response
    {
        $this->logger->info('Disabling merchant venue mapping', ['id' => $merchantVenue->getId()]);
        $merchantVenue->setValid(false);

        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($request->headers->get('referer') ?? $this->generateUrl('mapping_index'));
    }

    #[Route('/switch/{merchant}/{venue}', name: 'mapping_switch', methods: ['GET'])]
    public function switch(Request $request, Merchant $merchant, Venue $venue): Response
    {
        $merchantVenue = $merchant->getVenue();
        $oldVenue = $merchantVenue?->getVenue();
        $this->logger->info('Switching merchant venue mapping', [
            'merchant_id' => $merchant->getId(),
            'mapping_id' => $merchantVenue?->getId(),
            'old_venue_id' => $oldVenue?->getId(),
            'new_venue_id' => $venue->getId(),
        ]);

        $em = $this->getDoctrine()->getManager();

        $merchantVenue = $merchant->getVenue();
        if ($merchantVenue === null) {
            $this->logger->debug('Merchant was missing mapping, creating new one');
            $merchantVenue = new MerchantVenue();
            $merchantVenue->setMerchant($merchant);
            $merchantVenue->setVenue($venue);
            $merchantVenue->setValid(true);

            $em->persist($merchantVenue);
        } else {
            $merchantVenue->setVenue($venue);
            $merchantVenue->setValid(true);
        }

        $em->flush();

        return $this->redirect($request->headers->get('referer') ?? $this->generateUrl('mapping_index'));
    }
}
