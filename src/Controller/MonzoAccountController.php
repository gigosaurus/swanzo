<?php

declare(strict_types=1);

namespace App\Controller;

use App\ApiEntity\Monzo\Account;
use App\Entity\MonzoAccount;
use App\Repository\ApiClientRepository;
use App\Repository\MonzoAccountRepository;
use App\Utils\MonzoApi;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[Route('/monzo/account')]
class MonzoAccountController extends AbstractController
{
    private ApiClientRepository $apiClientRepository;
    private LoggerInterface $logger;

    public function __construct(ApiClientRepository $apiClientRepository, LoggerInterface $logger)
    {
        $this->apiClientRepository = $apiClientRepository;
        $this->logger = $logger;
    }

    protected function getMonzoApi(): MonzoApi
    {
        $monzoApi = $this->apiClientRepository->getMonzoApi();
        if ($monzoApi === null) {
            throw new BadRequestHttpException("Cannot configure webhook before configuring Monzo!");
        }
        return $monzoApi;
    }

    #[Route('/', name: 'monzo_account_index', methods: ['GET'])]
    public function index(MonzoAccountRepository $monzoAccountRepository): Response
    {
        $this->logger->debug('Displaying Monzo accounts');
        $monzoAccounts = $monzoAccountRepository->findAll();

        if (empty($monzoAccounts)) {
            $this->fetchAndUpdateAccounts();
            $monzoAccounts = $monzoAccountRepository->findAll();
        }

        return $this->render('monzo_account/index.html.twig', [
            'monzo_accounts' => $monzoAccounts,
        ]);
    }

    #[Route('/refresh', name: 'monzo_account_refresh', methods: ['GET'])]
    public function refresh(MonzoAccountRepository $monzoAccountRepository): Response
    {
        $this->logger->info('Manually refreshing Monzo accounts');
        $this->fetchAndUpdateAccounts($monzoAccountRepository->findAll());
        return $this->redirectToRoute('monzo_account_index');
    }

    #[Route('/{id}/enable', name: 'monzo_account_enable', methods: ['GET'])]
    public function enable(MonzoAccount $monzoAccount): Response
    {
        $this->logger->info('Enabling Monzo account', ['account' => $monzoAccount->getAccountId()]);
        $monzoApi = $this->getMonzoApi();
        $res = $monzoApi->registerWebhook($this->generateUrl(
            'monzo-webhook',
            referenceType: UrlGeneratorInterface::ABSOLUTE_URL,
        ), $monzoAccount->getAccountId());
        $monzoAccount->setWebhookId($res->webhook?->id);

        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('monzo_account_index');
    }

    #[Route('/{id}/disable', name: 'monzo_account_disable', methods: ['GET'])]
    public function disable(MonzoAccount $monzoAccount): Response
    {
        $this->logger->info('Disabling Monzo account', ['account' => $monzoAccount->getAccountId()]);
        $webhook = $monzoAccount->getWebhookId();
        if ($webhook !== null) {
            $monzoApi = $this->getMonzoApi();
            $monzoApi->deleteWebhook($webhook);
            $monzoAccount->setWebhookId(null);

            $this->getDoctrine()->getManager()->flush();
        }
        return $this->redirectToRoute('monzo_account_index');
    }

    /**
     * @param MonzoAccount[] $currentMonzoAccounts
     * @throws GuzzleException
     */
    protected function fetchAndUpdateAccounts(array $currentMonzoAccounts = []): void
    {
        $monzoApi = $this->getMonzoApi();
        $accounts = $monzoApi->listAccounts()->accounts;
        $em = $this->getDoctrine()->getManager();
        foreach ($accounts ?? [] as $account) {
            if (
                empty(array_filter(
                    $currentMonzoAccounts,
                    static fn(MonzoAccount $monzoAccount) => $monzoAccount->getAccountId() === $account->id
                ))
            ) {
                $monzoAccount = new MonzoAccount();
                $monzoAccount->setAccountId($account->id);
                $monzoAccount->setDescription($this->createDescription($account));
                $monzoAccount->setType($account->type);
                $em->persist($monzoAccount);
            }
        }
        $em->flush();
    }

    protected function createDescription(Account $account): string
    {
        $description = $account->description;
        foreach ($account->owners as $owner) {
            $description = str_replace($owner->user_id, $owner->preferred_name, $description);
        }
        return $description;
    }
}
