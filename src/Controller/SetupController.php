<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\ApiClient;
use App\Entity\ApiClientCredentials;
use App\Entity\User;
use App\Repository\ApiClientRepository;
use App\Repository\UserRepository;
use App\Utils\MonzoApi;
use App\Utils\SwarmApi;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SetupController extends AbstractController
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    #[Route('/setup', name: 'setup', methods: ['GET'])]
    public function index(UserRepository $userRepository): Response
    {
        if ($userRepository->count([]) > 0) {
            $this->logger->info('Setup already complete');
            return $this->redirectToRoute('app_login');
        }

        $this->logger->info('Displaying setup page');
        return $this->render('setup/index.html.twig');
    }

    #[Route('/setup', name: 'setup-submit', methods: ['POST'])]
    public function setup(
        Request $request,
        UserRepository $userRepository,
        UserPasswordHasherInterface $passwordHasher,
    ): Response {
        if ($userRepository->count([]) > 0) {
            $this->logger->warning('Setup already complete upon POST');
            return $this->redirectToRoute('app_login');
        }

        $username = (string) $request->request->get('username', 'admin');
        $password = (string) $request->request->get('password', 'password');

        $this->logger->info('Setting up admin user', ['user' => $username, ['pass' => $password]]);

        // create the admin user
        $adminUser = new User();
        $adminUser->setUsername($username);
        $adminUser->setPassword($passwordHasher->hashPassword($adminUser, $password));

        $em = $this->getDoctrine()->getManager();
        $em->persist($adminUser);
        $em->flush();

        return $this->redirectToRoute('configure');
    }

    #[Route('/configure', name: 'configure', methods: ['GET'])]
    public function configure(ApiClientRepository $apiClientRepository): Response
    {
        $this->logger->debug('Displaying configuration page');
        $monzo = $apiClientRepository->findMonzoClient();
        $swarm = $apiClientRepository->findSwarmClient();
        return $this->render('setup/apis.html.twig', [
            'showMonzo' => $monzo === null,
            'monzo' => $monzo?->getClientId(),
            'showSwarm' => $monzo !== null && $swarm === null,
            'swarm' => $swarm?->getClientId(),
            'showAccountSetup' => $monzo !== null && $swarm !== null,
        ]);
    }

    #[Route('/configure', name: 'configure-submit', methods: ['POST'])]
    public function setupApiSubmit(Request $request): Response
    {
        $this->logger->debug('Setting up an API');
        $type = (string) $request->request->get('type', '');
        if (!in_array($type, ['monzo', 'swarm'], true)) {
            $this->logger->warning('Invalid API client type!');
            throw new BadRequestHttpException('Invalid API client type!');
        }

        $this->logger->info('Creating API client', ['type' => $type]);
        $id = (string) $request->request->get('clientId', '');
        $secret = (string) $request->request->get('clientSecret', '');

        $client = new ApiClient();
        $client->setClientId($id);
        $client->setClientSecret($secret);
        $client->setService($type);
        $client->setState(bin2hex(random_bytes(8)));

        $em = $this->getDoctrine()->getManager();
        $em->persist($client);
        $em->flush();

        $this->logger->info('Redirecting user to external OAuth authentication');
        return match ($client->getService()) {
            'monzo' => $this->redirect('https://auth.monzo.com/?' . http_build_query([
                    'client_id' => $client->getClientId(),
                    'redirect_uri' => $this->generateRedirectUrl('redirect-monzo'),
                    'response_type' => 'code',
                    'state' => $client->getState(),
                ])),
            'swarm' => $this->redirect('https://foursquare.com/oauth2/authenticate?' . http_build_query([
                    'client_id' => $client->getClientId(),
                    'redirect_uri' => $this->generateRedirectUrl('redirect-swarm'),
                    'response_type' => 'code',
                ])),
        };
    }

    #[Route('/api/redirect-monzo', name: 'redirect-monzo', methods: ['GET'])]
    public function redirectMonzo(Request $request, ApiClientRepository $apiClientRepository): Response
    {
        $this->logger->info('Handling OAuth request back from Monzo');
        $apiClient = $apiClientRepository->findMonzoClient(false, $request->query->get('state'));
        if ($apiClient === null) {
            $this->logger->warning('Missing Monzo client!');
            throw new NotFoundHttpException();
        }

        $responseData = (new MonzoApi($this->getDoctrine()->getManager(), $apiClient, $this->logger))
            ->authenticate($this->generateRedirectUrl('redirect-monzo'), $request->query->get('code', ''));

        $this->logger->info('Creating new Monzo credentials');
        $this->createCredentials($apiClient, $responseData->access_token ?? '', $responseData->refresh_token ?? '');
        return $this->redirectToRoute('configure');
    }

    #[Route('/api/redirect-swarm', name: 'redirect-swarm', methods: ['GET'])]
    public function redirectSwarm(Request $request, ApiClientRepository $apiClientRepository): Response
    {
        $this->logger->info('Handling OAuth request back from Swarm');
        $apiClient = $apiClientRepository->findSwarmClient(false);
        if ($apiClient === null) {
            $this->logger->warning('Missing Swarm client!');
            throw new NotFoundHttpException();
        }

        $responseData = (new SwarmApi($apiClient, $this->logger, 'https://foursquare.com/'))
            ->authenticate($this->generateRedirectUrl('redirect-swarm'), $request->query->get('code', ''));

        $this->logger->info('Creating new Swarm credentials');
        $this->createCredentials($apiClient, $responseData->access_token ?? '');
        return $this->redirectToRoute('configure');
    }

    private function createCredentials(ApiClient $apiClient, string $accessToken, ?string $refreshToken = null): void
    {
        $credentials = new ApiClientCredentials();
        $credentials->setClient($apiClient);
        $credentials->setAccessToken($accessToken);
        $credentials->setRefreshToken($refreshToken);

        $apiClient->setActive(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($credentials);
        $em->flush();
    }

    protected function generateRedirectUrl(string $routeName): string
    {
        return $this->generateUrl($routeName, referenceType: UrlGeneratorInterface::ABSOLUTE_URL);
    }
}
