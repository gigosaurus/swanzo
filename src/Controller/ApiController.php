<?php

declare(strict_types=1);

namespace App\Controller;

use App\ApiEntity\Monzo\Merchant as MonzoMerchant;
use App\ApiEntity\Monzo\TransactionCreated;
use App\ApiEntity\Monzo\WebhookPayload;
use App\ApiEntity\Swarm\Checkin;
use App\ApiEntity\Swarm\Venue as SwarmVenue;
use App\Entity\Merchant;
use App\Entity\MerchantVenue;
use App\Entity\Variable;
use App\Entity\Venue;
use App\Repository\ApiClientRepository;
use App\Repository\MerchantRepository;
use App\Repository\VariableRepository;
use App\Repository\VenueRepository;
use App\Serializer\ApiEntityDenormalizer;
use App\Utils\SwarmApi;
use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Throwable;
use UnexpectedValueException;

class ApiController extends AbstractController
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    #[Route('/api/monzo-webhook', name: 'monzo-webhook')]
    public function index(
        Request $request,
        MerchantRepository $merchantRepository,
        VenueRepository $venueRepository,
        ApiClientRepository $apiClientRepository,
        VariableRepository $variableRepository,
    ): Response {
        try {
            $transaction = $this->transform($request);

            if (
                $transaction->merchant === null // no merchant = no place to check-in to
                || $transaction->merchant->atm // atm transactions are not places to check-in to
                || $transaction->originator // if the user is the originator, they're not spending money somewhere
                || $transaction->is_load // "load" = top-up, these are also not places to check-in to
                || !$transaction->include_in_spending // e.g. failed transaction or card check
            ) {
                $this->logger->debug('Ignoring transaction');
                throw new InvalidArgumentException();
            }

            $merchant = $this->getUpdatedMerchant($transaction->merchant, $merchantRepository);

            $swarmApi = $apiClientRepository->getSwarmApi();
            if ($swarmApi === null) {
                $this->logger->warning('Swarm API has not been configured');
                throw new UnexpectedValueException();
            }

            $venue = $this->createMerchantVenueMapping($merchant, $swarmApi, $variableRepository, $venueRepository);
            if (!$merchant->getVenue()?->getValid()) {
                $this->logger->debug('Merchant venue mapping is marked as invalid!');
                throw new InvalidArgumentException();
            }

            $this->logger->debug('Flushing entity changes');
            $this->getDoctrine()->getManager()->flush();

            $this->attemptCheckin($swarmApi, $venue, $variableRepository->get(Variable::CHECKIN_COOLDOWN));
        } catch (UnexpectedValueException) {
            $this->getDoctrine()->getManager()->flush();
        } catch (InvalidArgumentException) {
            // do nothing
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage());
        }

        $this->logger->debug('Message processing finished');
        return $this->json(null, 204);
    }

    private function transform(Request $request): TransactionCreated
    {
        $content = $request->getContent();
        $this->logger->info('Monzo webhook received a message', ['message' => $content]);

        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        $data = $serializer->deserialize($content, WebhookPayload::class, 'json');

        if ($data->type !== 'transaction.created' || $data->data === null) {
            throw new Exception("Unexpected event type");
        }

        return $data->data;
    }

    private function getUpdatedMerchant(MonzoMerchant $monzoMerchant, MerchantRepository $merchantRepository,): Merchant
    {
        $merchant = $merchantRepository->findOneByMerchantId($monzoMerchant->id);
        if ($merchant === null) {
            $this->logger->info('Creating new merchant');
            $merchant = Merchant::fromApiResponse($monzoMerchant);
            $this->getDoctrine()->getManager()->persist($merchant);
        } else {
            $this->logger->debug('Using pre-existing merchant');
            $merchant->updateFromData($monzoMerchant);
        }
        return $merchant;
    }

    private function getUpdatedVenue(SwarmVenue $swarmVenue, VenueRepository $venueRepository): Venue
    {
        $venue = $venueRepository->findOneByVenueId($swarmVenue->id);
        if ($venue === null) {
            $this->logger->debug('Creating new venue');
            $venue = Venue::fromApiResponse($swarmVenue);
            $this->getDoctrine()->getManager()->persist($venue);
        } else {
            $this->logger->info('Using pre-existing merchant');
            $venue->updateFromData($swarmVenue);
        }
        return $venue;
    }

    private function createMerchantVenueMapping(
        Merchant $merchant,
        SwarmApi $swarmApi,
        VariableRepository $variableRepository,
        VenueRepository $venueRepository,
    ): Venue {
        $merchantVenue = $merchant->getVenue();
        if ($merchantVenue !== null) {
            $this->logger->debug('Merchant already has venue mapping');
            return $merchantVenue->getVenue();
        }

        $this->logger->debug('Merchant does not have venue mapping');
        $merchantAddress = $merchant->getAddress();
        if ($merchantAddress === null) {
            $this->logger->error('Missing merchant address!', [
                'id' => $merchant->getId(),
                'merchant' => $merchant->getMerchantId(),
            ]);
            throw new UnexpectedValueException();
        }

        $response = $swarmApi->venueSearch(
            $merchantAddress->getLatitude(),
            $merchantAddress->getLongitude(),
            $merchant->getName(),
            $variableRepository->get(Variable::MAX_VENUE_DISTANCE_FROM_MERCHANT)
        );

        $venues = $response->response?->venues ?? [];
        $swarmVenue = reset($venues) ?: null;
        if ($swarmVenue === null) {
            $this->logger->warning('No venues found for the merchant');
            throw new UnexpectedValueException();
        }

        $venue = $this->getUpdatedVenue($swarmVenue, $venueRepository);

        $this->logger->info('Creating new merchant venue mapping');
        $merchantVenue = (new MerchantVenue())
            ->setMerchant($merchant)
            ->setVenue($venue)
            ->setValid(true);

        $this->getDoctrine()->getManager()->persist($merchantVenue);

        $merchant->setVenue($merchantVenue);
        return $venue;
    }

    protected function attemptCheckin(SwarmApi $swarmApi, Venue $venue, int $cooldown): void
    {
        $checkins = $swarmApi->getCheckins(time() - $cooldown);
        if (
            !empty(array_filter(
                $checkins->response?->items ?? [],
                static fn(Checkin $checkin): bool => $checkin->venue->id === $venue->getVenueId()
            ))
        ) {
            $this->logger->debug('Already recently checked in to venue!');
            return;
        }

        $swarmApi->checkin($venue->getVenueId());
    }
}
