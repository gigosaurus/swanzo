<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Variable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use InvalidArgumentException;

/**
 * @method Variable|null find($id, $lockMode = null, $lockVersion = null)
 * @method Variable|null findOneBy(array $criteria, array $orderBy = null)
 * @method Variable[]    findAll()
 * @method Variable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VariableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Variable::class);
    }

    public function get(string $key): int
    {
        if (!array_key_exists($key, Variable::DEFAULTS)) {
            throw new InvalidArgumentException(sprintf("Invalid variable key: %s", $key));
        }

        /** @var Variable|null */
        $variable = $this->createQueryBuilder('variable')
            ->andWhere('variable.key = :key')
            ->setParameter('key', $key)
            ->getQuery()
            ->getOneOrNullResult();

        if ($variable === null) {
            $variable = new Variable();
            $variable->setKey($key);
            $variable->setValue(Variable::DEFAULTS[$key]);
            $this->_em->persist($variable);
            $this->_em->flush();
        }

        return $variable->getValue();
    }
}
