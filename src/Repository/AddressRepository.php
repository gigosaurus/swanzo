<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Address;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

/**
 * @method Address|null find($id, $lockMode = null, $lockVersion = null)
 * @method Address|null findOneBy(array $criteria, array $orderBy = null)
 * @method Address[]    findAll()
 * @method Address[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddressRepository extends ServiceEntityRepository
{
    private LoggerInterface $logger;

    public function __construct(ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, Address::class);
        $this->logger = $logger;
    }

    /**
     * @return Address[]
     */
    public function findNearby(Address $address, int $radius = 50): array
    {
        $this->logger->debug('Finding nearby address', [
            'address' => ['lat' => $address->getLatitude(), 'lng' => $address->getLongitude()],
        ]);

        $diff = $radius / 111120; // 1° = 111,120m
        $qb = $this->createQueryBuilder('a');

        $lat1 = $address->getLatitude() - $diff;
        $lat2 = $address->getLatitude() + $diff;
        if ($lat1 < -180) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->gte('a.latitude', $lat1 + 360),
                $qb->expr()->between('a.latitude', -180, (string) $lat2)
            ));
        } elseif ($lat2 > 180) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->lte('a.latitude', $lat2 - 360),
                $qb->expr()->between('a.latitude', (string) $lat1, 180)
            ));
        } else {
            $qb->andWhere($qb->expr()->between('a.latitude', (string) $lat1, (string) $lat2));
        }

        $lng1 = $address->getLongitude() - $diff;
        $lng2 = $address->getLongitude() + $diff;
        if ($lng1 < -90) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->gte('a.longitude', $lng1 + 180),
                $qb->expr()->between('a.longitude', -90, (string) $lng2)
            ));
        } elseif ($lng2 > 90) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->lte('a.longitude', $lng2 - 180),
                $qb->expr()->between('a.longitude', (string) $lng1, 90)
            ));
        } else {
            $qb->andWhere($qb->expr()->between('a.longitude', (string) $lng1, (string) $lng2));
        }

        $query = $qb->getQuery();
        $this->logger->debug('Generated query', ['sql' => $query->getSQL()]);
        /** @var Address[] */
        return $query->getResult();
    }
}
