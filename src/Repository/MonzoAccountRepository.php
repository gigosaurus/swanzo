<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\MonzoAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MonzoAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method MonzoAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method MonzoAccount[]    findAll()
 * @method MonzoAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MonzoAccountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MonzoAccount::class);
    }
}
