<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Venue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Venue|null find($id, $lockMode = null, $lockVersion = null)
 * @method Venue|null findOneBy(array $criteria, array $orderBy = null)
 * @method Venue[]    findAll()
 * @method Venue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VenueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Venue::class);
    }

    public function findOneByVenueId(string $venueId): ?Venue
    {
        /** @var Venue|null */
        return $this->createQueryBuilder('venue')
            ->andWhere('venue.venueId = :id')
            ->setParameter('id', $venueId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string[] $ids
     * @return Venue[]
     */
    public function findByIds(array $ids): array
    {
        $qb = $this->createQueryBuilder('venue');
        $qb->andWhere($qb->expr()->in('venue.venueId', $ids));
        /** @var Venue[] */
        return $qb->getQuery()->getResult();
    }
}
