<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ApiClient;
use App\Utils\MonzoApi;
use App\Utils\SwarmApi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

/**
 * @method ApiClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiClient[]    findAll()
 * @method ApiClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApiClientRepository extends ServiceEntityRepository
{
    private LoggerInterface $logger;

    public function __construct(ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, ApiClient::class);
        $this->logger = $logger;
    }

    private function findOneClient(string $service, bool $active = true, ?string $state = null): ?ApiClient
    {
        $qb = $this->createQueryBuilder('api_client');
        $qb
            ->where($qb->expr()->eq('api_client.service', ':service'))->setParameter(':service', $service)
            ->andWhere($qb->expr()->eq('api_client.active', ':active'))->setParameter(':active', $active);
        if ($state !== null) {
            $qb->andWhere($qb->expr()->eq('api_client.state', ':state'))->setParameter(':state', $state);
        }
        /** @var ApiClient|null */
        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }

    public function findMonzoClient(bool $active = true, ?string $state = null): ?ApiClient
    {
        return $this->findOneClient('monzo', $active, $state);
    }

    public function findSwarmClient(bool $active = true): ?ApiClient
    {
        return $this->findOneClient('swarm', $active);
    }

    public function getSwarmApi(): ?SwarmApi
    {
        $swarmClient = $this->findSwarmClient();
        if (
            $swarmClient === null
            || $swarmClient->getActive() !== true
            || $swarmClient->getCredentials()?->getAccessToken() === null
        ) {
            $this->logger->warning('Failed to retrieve swarm client');
            return null;
        }
        return new SwarmApi($swarmClient, $this->logger);
    }

    public function getMonzoApi(): ?MonzoApi
    {
        $monzoClient = $this->findMonzoClient();
        if (
            $monzoClient === null
            || $monzoClient->getActive() !== true
            || $monzoClient->getCredentials()?->getAccessToken() === null
            || $monzoClient->getCredentials()?->getRefreshToken() === null
        ) {
            $this->logger->warning('Failed to retrieve monzo client');
            return null;
        }
        return new MonzoApi($this->_em, $monzoClient, $this->logger);
    }
}
