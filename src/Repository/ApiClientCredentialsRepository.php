<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ApiClientCredentials;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApiClientCredentials|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiClientCredentials|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiClientCredentials[]    findAll()
 * @method ApiClientCredentials[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApiClientCredentialsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApiClientCredentials::class);
    }
}
