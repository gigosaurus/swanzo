<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Merchant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Merchant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Merchant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Merchant[]    findAll()
 * @method Merchant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MerchantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Merchant::class);
    }

    public function findOneByMerchantId(string $merchantId): ?Merchant
    {
        /** @var Merchant|null */
        return $this->createQueryBuilder('m')
            ->andWhere('m.merchantId = :mid')
            ->setParameter('mid', $merchantId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
