<?php

declare(strict_types=1);

namespace App\Utils;

use App\ApiEntity\Swarm\SwarmApiResponse;
use App\Entity\ApiClient;
use App\Serializer\ApiEntityDenormalizer;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

final class SwarmApi
{
    private const BASE_URI = 'https://api.foursquare.com/v2/';
    private const VERSION = '20211001';

    private ApiClient $apiClient;
    private LoggerInterface $logger;
    private string $baseUri;

    public function __construct(ApiClient $apiClient, LoggerInterface $logger, string $baseUri = self::BASE_URI)
    {
        $this->apiClient = $apiClient;
        $this->logger = $logger;
        $this->baseUri = $baseUri;
    }

    public function authenticate(string $redirectUri, string $code): SwarmApiResponse
    {
        return $this->makeGetRequest('oauth2/access_token', [
            'grant_type' => 'authorization_code',
            'client_id' => $this->apiClient->getClientId(),
            'client_secret' => $this->apiClient->getClientSecret(),
            'redirect_uri' => $redirectUri,
            'code' => $code,
        ], auth: false);
    }

    public function venueSearch(
        float $latitude,
        float $longitude,
        string $name = '',
        int $radius = 50,
        int $limit = 1,
    ): SwarmApiResponse {
        return $this->makeGetRequest('venues/search', [
            'll' => $latitude . ',' . $longitude,
            'radius' => $radius,
            'query' => $name,
            'limit' => $limit,
        ]);
    }

    public function checkin(string $venueId): SwarmApiResponse
    {
        return $this->makePostRequest('checkins/add', [
            'venueId' => $venueId,
            'shout' => 'Auto check-in from Monzo transaction',
        ]);
    }

    public function getCheckins(int $after): SwarmApiResponse
    {
        return $this->makeGetRequest('users/self/checkins', [
            'limit' => 10,
            'sort' => 'newestfirst',
            'afterTimestamp' => $after,
        ]);
    }

    /**
     * @param array<string, array<string, string>> $options
     * @throws GuzzleException
     */
    private function makeRequest(
        string $method,
        string $uri,
        array $queryParams = [],
        array $options = [],
        bool $auth = true,
    ): SwarmApiResponse {
        $credentials = $this->apiClient->getCredentials();
        if ($auth && $credentials !== null) {
            $queryParams += [
                'oauth_token' => $credentials->getAccessToken(),
                'v' => self::VERSION,
            ];
        }
        if (!empty($queryParams)) {
            $uri .= '?' . http_build_query($queryParams);
        }
        $guzzleClient = new Client(['base_uri' => $this->baseUri]);
        $options['headers'] = ($options['headers'] ?? []) + ['Accept' => 'application/json'];

        $this->logger->debug(sprintf('Making %s request to %s', $method, $uri));
        $guzzleResponse = $guzzleClient->request($method, $uri, $options);

        $response = $guzzleResponse->getBody()->getContents();
        $this->logger->debug('Received response', ['response' => $response]);

        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        /** @var SwarmApiResponse */
        return $serializer->deserialize($response, SwarmApiResponse::class, 'json');
    }

    /**
     * @param array<string, array<string, string>> $options
     * @throws GuzzleException
     */
    private function makeGetRequest(
        string $uri,
        array $queryParams = [],
        array $options = [],
        bool $auth = true,
    ): SwarmApiResponse {
        return $this->makeRequest('GET', $uri, $queryParams, $options, $auth);
    }

    /**
     * @param array<string, string> $params
     * @throws GuzzleException
     */
    private function makePostRequest(string $uri, array $params): SwarmApiResponse
    {
        return $this->makeRequest('POST', $uri, [], ['form_params' => $params]);
    }
}
