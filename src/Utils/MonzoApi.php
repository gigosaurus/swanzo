<?php

declare(strict_types=1);

namespace App\Utils;

use App\ApiEntity\Monzo\MonzoApiResponse;
use App\Entity\ApiClient;
use App\Serializer\ApiEntityDenormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

final class MonzoApi
{
    private const BASE_URI = 'https://api.monzo.com/';

    private EntityManagerInterface|ObjectManager $entityManager;
    private ApiClient $apiClient;
    private LoggerInterface $logger;

    public function __construct(
        EntityManagerInterface|ObjectManager $entityManager,
        ApiClient $apiClient,
        LoggerInterface $logger,
    ) {
        $this->entityManager = $entityManager;
        $this->apiClient = $apiClient;
        $this->logger = $logger;
    }

    /**
     * @throws GuzzleException
     */
    public function authenticate(string $redirectUri, string $code): MonzoApiResponse
    {
        return $this->makePostRequest('oauth2/token', [
            'grant_type' => 'authorization_code',
            'client_id' => $this->apiClient->getClientId(),
            'client_secret' => $this->apiClient->getClientSecret(),
            'redirect_uri' => $redirectUri,
            'code' => $code,
        ], false, false);
    }

    /**
     * @throws GuzzleException
     */
    public function listAccounts(): MonzoApiResponse
    {
        return $this->makeGetRequest('accounts');
    }

    /**
     * @throws GuzzleException
     */
    public function registerWebhook(string $url, string $accountId): MonzoApiResponse
    {
        return $this->makePostRequest('webhooks', ['account_id' => $accountId, 'url' => $url]);
    }

    /**
     * @throws GuzzleException
     */
    public function deleteWebhook(string $webhookId): MonzoApiResponse
    {
        return $this->makeDeleteRequest('webhooks/' . $webhookId);
    }

    /**
     * @param array<string, array<string, string>> $options
     * @throws GuzzleException
     */
    private function makeRequest(
        string $method,
        string $uri,
        array $options = [],
        bool $auth = true,
        bool $retry = true,
    ): MonzoApiResponse {
        $guzzleClient = new Client(['base_uri' => self::BASE_URI]);
        $options['headers'] = ($options['headers'] ?? []) + ['Accept' => 'application/json'];
        $accessToken = $this->apiClient->getCredentials()?->getAccessToken();
        if ($auth && $accessToken !== null) {
            $options['headers']['authorization'] = 'Bearer ' . $accessToken;
        }
        try {
            $this->logger->debug(sprintf('Making %s request to %s', $method, $uri));
            $guzzleResponse = $guzzleClient->request($method, $uri, $options);
        } catch (ClientException $e) {
            $this->logger->warning('Received a client exception from the Monzo API');
            if ($e->getCode() === 401) {
                $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
                $response = $serializer->deserialize(
                    $e->getResponse()->getBody()->getContents(),
                    MonzoApiResponse::class,
                    'json'
                );
                if ($response->code === 'forbidden.verification_required') {
                    throw new RuntimeException("Verification required!");
                }
                if (!$retry) {
                    throw new RuntimeException(json_encode($options));
                }
                $this->refreshOAuth();
                return $this->makeRequest($method, $uri, $options, $auth, false);
            }
            if ($e->getCode() === 403) {
                throw new RuntimeException("Re-verification required!");
            }
            throw $e;
        }

        $response = $guzzleResponse->getBody()->getContents();
        $this->logger->debug('Received response', ['response' => $response]);

        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        return $serializer->deserialize($response, MonzoApiResponse::class, 'json');
    }

    /**
     * @throws GuzzleException
     */
    private function makeGetRequest(string $uri, bool $auth = true, bool $retry = true): MonzoApiResponse
    {
        return $this->makeRequest('GET', $uri, [], $auth, $retry);
    }

    /**
     * @param array<string, string> $params
     * @throws GuzzleException
     */
    private function makePostRequest(
        string $uri,
        array $params,
        bool $auth = true,
        bool $retry = true,
    ): MonzoApiResponse {
        return $this->makeRequest('POST', $uri, ['form_params' => $params], $auth, $retry);
    }

    /**
     * @param array<string, string> $params
     * @throws GuzzleException
     */
    private function makeDeleteRequest(
        string $uri,
        ?array $params = null,
        bool $auth = true,
        bool $retry = true,
    ): MonzoApiResponse {
        return $this->makeRequest(
            'DELETE',
            $uri,
            $params === null ? [] : ['form_params' => $params],
            $auth,
            $retry
        );
    }

    /**
     * @throws GuzzleException
     */
    private function refreshOAuth(): void
    {
        $this->logger->info('Attempting to refresh Monzo OAuth tokens');
        $credentials = $this->apiClient->getCredentials();
        if ($credentials === null) {
            throw new RuntimeException("Api client has no credentials!");
        }
        $response = $this->makePostRequest('oauth2/token', [
            'grant_type' => 'refresh_token',
            'client_id' => $this->apiClient->getClientId(),
            'client_secret' => $this->apiClient->getClientSecret(),
            'refresh_token' => $credentials->getRefreshToken() ?? '',
        ], false, false);
        $credentials->setAccessToken($response->access_token ?? '');
        $credentials->setRefreshToken($response->refresh_token ?? '');
        $this->logger->info('New tokens successfully acquired!');
        $this->entityManager->flush();
    }
}
