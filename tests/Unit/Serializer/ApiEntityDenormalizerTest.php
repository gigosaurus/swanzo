<?php

declare(strict_types=1);

namespace App\Tests\Unit\Serializer;

use App\Serializer\ApiEntityDenormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Serializer;
use UnexpectedValueException;

class ApiEntityDenormalizerTest extends TestCase
{
    /**
     * @return array<string, mixed>
     */
    private function createTestArray(): array
    {
        return [
            'foo' => 'bar',
            'int' => 1,
            'float' => 1.2,
            'neg_float' => -1.3029,
            'array' => [
                'nested' => [
                    'real' => [
                        [
                            'deep' => 'ta da',
                            'bool' => true,
                            'false' => false,
                        ],
                        [
                            'deep' => 'second',
                        ],
                    ],
                    'also' => 'here',
                ],
                'and' => 'here',
            ],
            'int_array' => [1, 2, 3, 4],
            'string_array' => ["a", "b", "c"],
            'nullable_array' => [1, 3, 8],
            'array_or_string' => 'string',
            'union_object' => [
                'deep' => [],
            ],
            'property_that_does_not_exist' => null,
        ];
    }

    public function testSerialize(): void
    {
        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        $obj = $serializer->deserialize(json_encode($this->createTestArray()), TestObject::class, 'json');

        $this->assertNotNull($obj);
        $this->assertInstanceOf(TestObject::class, $obj);
        $this->assertEquals('bar', $obj->foo);
        $this->assertEquals(1, $obj->int);
        $this->assertEquals(1.2, $obj->float);
        $this->assertEquals(-1.3029, $obj->neg_float);

        $this->assertInstanceOf(NestedTestObject::class, $obj->array);
        $this->assertEquals('here', $obj->array->and);

        $this->assertInstanceOf(NestedTestObject2::class, $obj->array->nested);
        $this->assertEquals('here', $obj->array->nested->also);

        $this->assertIsArray($obj->array->nested->real);
        $this->assertCount(2, $obj->array->nested->real);
        $this->assertArrayHasKey(0, $obj->array->nested->real);
        $this->assertArrayHasKey(1, $obj->array->nested->real);

        $this->assertInstanceOf(NestedTestObject3::class, $obj->array->nested->real[0]);
        $this->assertInstanceOf(NestedTestObject3::class, $obj->array->nested->real[1]);

        $this->assertEquals('ta da', $obj->array->nested->real[0]->deep);
        $this->assertEquals('second', $obj->array->nested->real[1]->deep);

        $this->assertTrue($obj->array->nested->real[0]->bool);
        $this->assertFalse($obj->array->nested->real[1]->bool);

        $this->assertFalse($obj->array->nested->real[0]->false);
        $this->assertNull($obj->array->nested->real[1]->false);

        $this->assertIsArray($obj->int_array);
        $this->assertIsArray($obj->string_array);

        $this->assertCount(4, $obj->int_array);
        $this->assertCount(3, $obj->string_array);
        $this->assertEquals([1, 2, 3, 4], $obj->int_array);
        $this->assertEquals(["a", "b", "c"], $obj->string_array);

        $this->assertEquals([1, 3, 8], $obj->nullable_array);
        $this->assertEquals('string', $obj->array_or_string);

        $this->assertInstanceOf(NestedTestObject2::class, $obj->union_object);

        $this->assertObjectNotHasAttribute('property_that_does_not_exist', $obj);
    }

    public function testInvalidSerialize(): void
    {
        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        $this->expectException(NotNormalizableValueException::class);
        $serializer->deserialize(json_encode(["null" => null, "foo" => ["a"]]), BrokenTestObject::class, 'json');
    }

    public function testInvalidSerializeObject(): void
    {
        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        $this->expectException(UnexpectedValueException::class);
        $serializer->deserialize(json_encode(["bar" => 3]), BrokenTestObject::class, 'json');
    }

    public function testInvalidSerializeBool(): void
    {
        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        $this->expectException(UnexpectedValueException::class);
        $serializer->deserialize(json_encode(["bool" => []]), BrokenTestObject::class, 'json');
    }

    public function testInvalidSerializeInt(): void
    {
        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        $this->expectException(UnexpectedValueException::class);
        $serializer->deserialize(json_encode(["int" => null]), BrokenTestObject::class, 'json');
    }

    public function testInvalidSerializeFloat(): void
    {
        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        $this->expectException(UnexpectedValueException::class);
        $serializer->deserialize(json_encode(["float" => []]), BrokenTestObject::class, 'json');
    }

    public function testInvalidSerializeString(): void
    {
        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        $this->expectException(UnexpectedValueException::class);
        $serializer->deserialize(json_encode(["string" => []]), BrokenTestObject::class, 'json');
    }

    public function testInvalidSerializeNull(): void
    {
        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        $this->expectException(UnexpectedValueException::class);
        $serializer->deserialize(json_encode(["null" => []]), BrokenTestObject::class, 'json');
    }

    public function testInvalidSerializeResource(): void
    {
        $serializer = new Serializer([new ApiEntityDenormalizer()], [new JsonEncoder()]);
        $this->expectException(UnexpectedValueException::class);
        $serializer->deserialize(json_encode(["resource" => []]), BrokenTestObject::class, 'json');
    }
}
