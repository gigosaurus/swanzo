<?php

declare(strict_types=1);

namespace App\Tests\Unit\Serializer;

class NestedTestObject2
{
    /** @var NestedTestObject3[] $real */
    public array $real = [];
    public string $also = '';
}
