<?php

declare(strict_types=1);

namespace App\Tests\Unit\Serializer;

use App\ApiEntity\ApiResponse;

class TestObject implements ApiResponse
{
    public string $foo = '';
    public int $int = 0;
    public float $float = 0;
    public float $neg_float = 0;
    public NestedTestObject $array;
    /** @var int[] $int_array */
    public array $int_array = [];
    /** @var string[] $string_array */
    public array $string_array = [];
    /** @var string[]|int[]|null  */
    public ?array $nullable_array = null;
    public array|string $array_or_string = [];
    public NestedTestObject3|NestedTestObject2|null $union_object = null;

    public function __construct()
    {
        $this->array = new NestedTestObject();
    }
}
