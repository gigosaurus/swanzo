<?php

declare(strict_types=1);

namespace App\Tests\Unit\Serializer;

use App\ApiEntity\ApiResponse;

class BrokenTestObject implements ApiResponse
{
    // phpcs:disable
    public $foo;
    // phpcs:enable
    public ?BrokenTestObject $bar = null;
    public bool $bool = true;
    public int $int = 0;
    public float $float = 0;
    public string $string = '';
    /** @var null $null */
    public $null;
    /** @var resource $resource */
    public $resource;
}
