<?php

declare(strict_types=1);

namespace App\Tests\Unit\Serializer;

class NestedTestObject
{
    public string $and = '';
    public NestedTestObject2 $nested;

    public function __construct()
    {
        $this->nested = new NestedTestObject2();
    }
}
