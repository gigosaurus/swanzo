<?php

declare(strict_types=1);

namespace App\Tests\Unit\Serializer;

class NestedTestObject3
{
    public string $deep = '';
    public bool $bool = false;
    public ?bool $false = null;
}
