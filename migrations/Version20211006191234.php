<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211006191234 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Initial Migration';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE address_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE api_client_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE api_client_credentials_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE merchant_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE merchant_venue_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE monzo_account_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE variable_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE venue_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE address (id INT NOT NULL, formatted VARCHAR(255) NOT NULL, address VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, region VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, postcode VARCHAR(255) DEFAULT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D4E6F814118D12385E16F6B ON address (latitude, longitude)');
        $this->addSql('CREATE TABLE api_client (id INT NOT NULL, credentials_id INT DEFAULT NULL, service VARCHAR(255) NOT NULL, client_id VARCHAR(255) NOT NULL, client_secret VARCHAR(255) NOT NULL, state VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_41B343D541E8B2E5 ON api_client (credentials_id)');
        $this->addSql('CREATE TABLE api_client_credentials (id INT NOT NULL, access_token VARCHAR(255) NOT NULL, refresh_token VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE merchant (id INT NOT NULL, address_id INT NOT NULL, merchant_id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, logo VARCHAR(255) DEFAULT NULL, emoji VARCHAR(5) DEFAULT NULL, category VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_74AB25E16796D554 ON merchant (merchant_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_74AB25E1F5B7AF75 ON merchant (address_id)');
        $this->addSql('CREATE TABLE merchant_venue (id INT NOT NULL, merchant_id INT NOT NULL, venue_id INT NOT NULL, valid BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_602C14A6796D554 ON merchant_venue (merchant_id)');
        $this->addSql('CREATE INDEX IDX_602C14A40A73EBA ON merchant_venue (venue_id)');
        $this->addSql('CREATE TABLE monzo_account (id INT NOT NULL, account_id VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, webhook_id VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON "user" (username)');
        $this->addSql('CREATE TABLE variable (id INT NOT NULL, key VARCHAR(255) NOT NULL, value INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CC4D878D8A90ABA9 ON variable (key)');
        $this->addSql('CREATE TABLE venue (id INT NOT NULL, address_id INT NOT NULL, venue_id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, category VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_91911B0D40A73EBA ON venue (venue_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_91911B0DF5B7AF75 ON venue (address_id)');
        $this->addSql('ALTER TABLE api_client ADD CONSTRAINT FK_41B343D541E8B2E5 FOREIGN KEY (credentials_id) REFERENCES api_client_credentials (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE merchant ADD CONSTRAINT FK_74AB25E1F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE merchant_venue ADD CONSTRAINT FK_602C14A6796D554 FOREIGN KEY (merchant_id) REFERENCES merchant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE merchant_venue ADD CONSTRAINT FK_602C14A40A73EBA FOREIGN KEY (venue_id) REFERENCES venue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE venue ADD CONSTRAINT FK_91911B0DF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE merchant DROP CONSTRAINT FK_74AB25E1F5B7AF75');
        $this->addSql('ALTER TABLE venue DROP CONSTRAINT FK_91911B0DF5B7AF75');
        $this->addSql('ALTER TABLE api_client DROP CONSTRAINT FK_41B343D541E8B2E5');
        $this->addSql('ALTER TABLE merchant_venue DROP CONSTRAINT FK_602C14A6796D554');
        $this->addSql('ALTER TABLE merchant_venue DROP CONSTRAINT FK_602C14A40A73EBA');
        $this->addSql('DROP SEQUENCE address_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE api_client_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE api_client_credentials_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE merchant_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE merchant_venue_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE monzo_account_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE variable_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE venue_id_seq CASCADE');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE api_client');
        $this->addSql('DROP TABLE api_client_credentials');
        $this->addSql('DROP TABLE merchant');
        $this->addSql('DROP TABLE merchant_venue');
        $this->addSql('DROP TABLE monzo_account');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE variable');
        $this->addSql('DROP TABLE venue');
    }
}
